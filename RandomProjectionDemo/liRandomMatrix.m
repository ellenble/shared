% A sparse random projection
% Ellen Le 2015
function R = liRandomMatrix(small, big, s)

% Our random SPARSE N x d matrix
% Entries are from 0, sqrt(s),-sqrt(s) with probability {1-1/s, 1/2s,
% 1/2s}
    
% Uses inverse transform method, not the best implementation
% but still fast

density = 1/s;

R = sprandn(small,big,density);  
R(find(R > 0)) = 1;
R(find(R < 0)) = -1;
R = (sqrt(s))*R;
end
