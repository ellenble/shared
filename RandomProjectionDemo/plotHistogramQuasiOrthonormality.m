% Histograms for GaussRPdemo.m, showing "almost orthonormality" of Gaussian vectors in high
% dimensions
% Ellen Le 2017

function [] = plotHistogramQuasiOrthonormality()
    for nDimension = [1E3,1E5,1E7]
        nExperiment = 20;
        nBin = 5;
        data = zeros(nExperiment,1);
        for iExperiment = 1:nExperiment
            data(iExperiment) = randprojdemo(nDimension);
        end
        hold on
        histogram(data,nBin)
    end
    legend('10E2','10E4','10E6')
end

% picks two random vectors on the unit sphere and takes dot prod
function randomDotProduct = randprojdemo(nDimension)
vector1 = randn(nDimension,1);
vector2 = randn(nDimension,1);
vector1Normalized = vector1/norm(vector1); 
vector2Normalized = vector2/norm(vector2);
randomDotProduct = vector1Normalized'*vector2Normalized;
end

