% Johnson-Lindstrauss Transform demo - norm preservation through a
% Gaussian projection and sparse variants
% Ellen Le 2017

%rng(9);
pause off
big = 1E6; % the limit for my wimpy laptop, try something bigger
             % if you can
small = 100;
dataVector = rand(big,1); %uniform, dense vector
normDataVector = norm(dataVector)

pause
%% A Gaussian random projection - from the Dasgupta and Gupta
%% constructive proof of the original JL-lemma 
R = (1/sqrt(small))*randn(small,big);
fR = @() norm(R*dataVector);
normR = fR()

pause
%% An Achlioptas (2/3rds) sparse random projection 
A =(1/sqrt(small))*liRandomMatrix(small,big,3);
fA = @() norm(A*dataVector);
normA = fA()

pause
% A 95-percent sparse random projection
S = (1/sqrt(small))*liRandomMatrix(small,big,20);
fS = @() norm(S*dataVector);
normS = fS()

pause
% A 99-percent sparse random projection
SS = (1/sqrt(small))*liRandomMatrix(small,big,100);
fSS = @() norm(SS*dataVector);
normSS = fSS()

pause
% note that faster multiplies are not the main benefit of sparse
% matrices (it's the overall complexity of our optimization), but it
% doesn't hurt to see that it is faster on 
% average:

timeR = timeit(fR)
timeA = timeit(fA)
timeS = timeit(fS)
timeSS = timeit(fSS)

pause
% storage:
whos R A S SS


figure('Position', [100, 100, 1049, 895])
subplot(2,2,1)
spy(R(1:10,1:10))
title('Gaussian')
set(gca,'FontSize',20)

subplot(2,2,2)
spy(A(1:10,1:10))
title('Achlioptas')
set(gca,'FontSize',20)

subplot(2,2,3)
spy(S(1:10,1:10))
title('95-percent sparse')
set(gca,'FontSize',20)

subplot(2,2,4)
spy(SS(1:10,1:10))
title('99-percent sparse')
set(gca,'FontSize',20)

