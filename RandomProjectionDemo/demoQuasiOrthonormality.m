% Demo showing "almost orthonormality" of Gaussian vectors in high
% dimensions
% Picks two random vectors on the unit sphere and takes dot product
% Ellen Le 2017

%rng(9) % seed the random generator if you want
pause on
nDimension = 1E3;
vector1 = randn(nDimension,1);
vector2 = randn(nDimension,1);
vector1Normalized = vector1/norm(vector1);
vector2Normalized = vector2/norm(vector2);
randomDotProduct = vector1Normalized'*vector2Normalized

pause

nDimension = 1E7;
vector1 = randn(nDimension,1);
vector2 = randn(nDimension,1);
vector1Normalized = vector1/norm(vector1); 
vector2Normalized = vector2/norm(vector2);
randomDotProduct = vector1Normalized'*vector2Normalized

figure('Position', [100, 100, 1049, 895])

tic;
plotHistogramQuasiOrthonormality()
toc;
set(gca,'FontSize',20)

pause
close all


